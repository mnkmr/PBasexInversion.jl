"""
    PBasexInversion
This package provides the functions to reconstruct the 3-dimensional
distribution from the corresponding projected 2-dimensional image using
**pBasex** method. [^G04]

# Usage
```julia-repl
julia> using PBasexInversion

julia> pbasex = PBasex(400)  # Build an inverter
PBasexInversion.PBasex[400x400]: l=[2], σ=2.00

julia> ret2 = pbasex(img, [200.5, 200.5]);  # Invert an projection image

julia> r, Ir = radialdist(ret);  # Obtain the radial distribution

julia> r, b = anisotropy(ret);  # Obtain the anisotropy parameters
```

# Save & Load an inverter
```julia-repl
julia> using PBasexInversion

julia> pbasex = PBasex(400)  # Build an inverter
PBasexInversion.PBasex[400x400]: l=[2], σ=2.00

julia> savepbasex("pbasex_400.h5", pbasex)

julia> pbasex_loaded = loadpbasex("pbasex_400.h5")
PBasexInversion.PBasex[400x400]: l=[2], σ=2.00
```

[^G04]: G. A. Garcia, L. Nahon and I. Powis, Two-dimensional charged particle image inversion using a polar basis function expansion, *Rev. Sci. Instrum.*, **2004**, 75, 4989–4996.
"""
module PBasexInversion

export
    PBasex,
    savepbasex,
    loadpbasex,
    statpbasex,
    crop,
    shear,
    average,
    smooth,
    radialdist,
    speeddist,
    energydist,
    anisotropy

using LinearAlgebra: Diagonal, svd, pinv, norm
using IonimageAnalysisCore:
    IonimageAnalysisCore,
    circumscribedsquare,
    legendre,
    average,
    crop,
    shear,
    smooth,
    radialdist,
    speeddist,
    energydist,
    anisotropy
using Printf: print, @printf, @sprintf
using ProgressMeter: Progress, next!
using QuadGK: quadgk


"""
    pbasex = PBasex(len::Integer;
                    l=[2], sigma::Real=2.0, σ::Real=sigma,
                    rstep::Real=sqrt(σ/2), astep::Real=1.0,
                    show_progress::Bool=true)

A callable object to reconstruct the 3D distribution from the projected image
by pBasex method. Each object has its fixed size and is applicable to a square
region of the image.

# Arguments
- `len`   : the length of an edge of inverted square.
- `l`     : the order of legendre polynomials included in the reconstruction.
            In many cases, it will be even numbers like [2], [2, 4].
            The zero-th order is always included.
- `σ`     : the width parameter of the radial Gaussian function of the basis
            set. Let `s` is the standard deviation of a Gaussian function,
            `σ` is `2s²`. See Eq. 2 of [^G04].
- `sigma` : an alias of `σ`.
- `rstep` : the radial step of polar grid for fitting in pixel unit.
- `astep` : the angular step of polar grid for fitting in degree unit.
- `show_progress`: show progress bar in basis-set calculation if true.

[^G04]: G. A. Garcia, L. Nahon and I. Powis, Two-dimensional charged particle image inversion using a polar basis function expansion, *Rev. Sci. Instrum.*, **2004**, 75, 4989–4996.

---

    pbasex(img::AbstractMatrix, center; fit::Symbol=:OLS,
           atol::Real=0.0, rtol::Real=n*eps())

Reconstruct the 3D distribution from the projection `img`. It returns a
[`PBasexImage`](@ref). `pbasex` is an instance of [`PBasex`](@ref).

# Arguments
- `img`   : the projection image as an array.
- `center`: the center coodinate of the projection, `(y, x)`. `y` and `x`
            should be a pair of integers or half-integers.
- `fit`   : the kind of least squares method to fit the image. (EXPERIMENTAL)
            `:OLS` uses ordinary least squares method which is faster, while
            `:WLS` uses weighted least squares method which is more robust for
            noisy images.
- `atol`  : it is used to invert a basis set matrix, G. See [`pinv`](@ref).
- `rtol`  : it is used to invert a basis set matrix, G. See [`pinv`](@ref).

# Example

``` julia-repl
julia> using PBasexInversion

julia> pbasex = PBasex(200)
PBasexInversion.PBasex[200x200]: l=[2], σ=2.00

julia> ret = pbasex(img, (100.5, 100.5));
```

!!! info "Schematic of geometry"
```
  ------------------------------------
  | img                              |
  |                len               |
  |         -----------------        |
  |         |     center    |        |
  |         |    (yc, xc)   |        |
yc|. . . . .|. . . .*       | len    |
  |         |       .       |        |
  |         |       .       |        |
  |         --------.--------        |
  |        inverted . region         |
  |                 .                |
  ------------------------------------
                    xc
```
"""
struct PBasex
    G⁺::Matrix{Float64}
    U::Matrix{Float64}
    S::Vector{Float64}
    V::Matrix{Float64}
    indices::Vector{CartesianIndex{2}}
    onaxis::Int
    len::Int
    rstep::Float64
    astep::Float64
    l::Vector{Int}
    σ::Float64
end
function PBasex(len::Integer;
                l=[2], sigma::Real=2.0, σ::Real=sigma,
                rstep::Real=sqrt(σ/2), astep::Real=1.0,
                show_progress::Bool=true)
    l = [0; filter(x -> x > 0, sort(unique(l)))]
    coords = sampling_coordinates(len, rstep, astep)
    G, indices, onaxis = gmatrix(coords, len, rstep, l, σ, show_progress)
    F = svd(G; full=false)
    U = F.U; S = F.S; V = F.V
    balance!(U, onaxis)
    G⁺ = V*Diagonal(1 ./ S)*U'
    PBasex(G⁺, U, S, V, indices, onaxis, len, rstep, astep, l, σ)
end

function (pbx::PBasex)(image0::AbstractMatrix{T}, center0;
                       fit::Symbol = :OLS,
                       atol::Real = 0.0,
                       rtol::Real = (eps(float(T))*minimum(size(pbx.U)))*iszero(atol)) where {T<:Real}
    len = pbx.len
    rstep = pbx.rstep
    astep = pbx.astep
    l = pbx.l
    σ = pbx.σ
    if !isinside(center0, image0)
        ArgumentError("The center coordinate $(center0) is outside of the $(size(image0, 1))×$(size(image0, 2)) image.") |> throw
    end
    if size(image0, 1) < len || size(image0, 2) < len
        ArgumentError("The basis set ($(len)×$(len)) is larger than the image ($(size(image0))).") |> throw
    end

    image, _ = circumscribedsquare(image0, center0, (len - 1)/2)
    if any(size(image) .< len)
        ArgumentError("The center $(center0) is inappropriate or the size of the basis set ($(len)×$(len)) is too big.") |> throw
    end

    R = radii(len, rstep)
    halfimage = twofold(image, dims=2, rev=true)./2
    P = halfimage[pbx.indices]
    C, varC = leastsquaresfit(fit, pbx, P, atol, rtol)
    C    = reshape(C, length(R), length(l))
    varC = reshape(varC, size(C))
    halfslice = to_cartesian(C, R, l, σ, len)
    slice = mirror(halfslice)
    PBasexImage(slice, len, rstep, astep, l, σ, C, varC)
end

function Base.show(io::IO, ::MIME"text/plain", z::PBasex)
    @printf("PBasexInversion.PBasex[%d×%d]: l=%s, σ=%.2f", z.len, z.len, z.l, z.σ)
end


"Return the radii of the original Newton sphere"
function radii(len::Integer, rstep::Real)
    R1 = iseven(len) ? 0.5 : 1.0
    R1:rstep:(len - 1)/2
end


"""
    sampling_coordinates(len, rstep, astep, l)

Return the sampling Cartesian coordinates of the projected image used for
fitting. The result is a `Vector{Tuple{Int,Int,Int,Int}}`, which item is
`(dz, dx, z, x, zs)` where `dz` and `dx` are the distance from the center as
the origin (0, 0) and `z` and `x` are the indices on an `(len+1)÷2` × `(len+1)÷2`
matrix. `zs` is also the corresponding index over the symmetry axis; if
`dz = ζ` at `(z, x)` then `dz = -ζ` at `(zs, x)`.
"""
function sampling_coordinates(len, rstep, astep)
    R = radii(len, rstep)
    θ = 0.0:astep:90.0
    z0 = (len + 1)/2
    x0 = iseven(len) ? 0.5 : 1.0
    cartpos(r, θ) = r*cosd(θ), r*sind(θ)
    coords = Vector{Tuple{Int,Int}}(undef, length(R)*length(θ))
    i = 0
    for R in R, θ in θ
        dz, dx = cartpos(R, θ)
        z = -round(Int, -z0 + dz, RoundNearestTiesUp)
        x =  round(Int,  x0 + dx, RoundNearestTiesUp)
        i += 1
        coords[i] = (z, x)
    end
    unique!(coords)
    map(c -> (z0 - c[1], c[2] - x0, c[1], c[2], len - c[1] + 1), coords)
end


"Compute the elements of G matrix by Eq.4"
function gmatrix(projection, len, rstep::Real, l, σ::Real, show_progress::Bool)
    coords = sort(projection)
    onaxis = count(c -> c[3] == c[5], projection)
    offaxis = length(coords) - onaxis
    ijmax = onaxis + 2*offaxis
    R = radii(len, rstep)
    kmax = length(R)
    p = Progress(kmax*length(l), 1, "Computing basis set...")
    G = zeros(Float64, ijmax, kmax, length(l))
    for (lidx, l) in enumerate(l), k in 1:kmax
        Rₖ = R[k]
        Threads.@threads for ij in eachindex(coords)
            z, x, _, _, _ = coords[ij]
            G[ij, k, lidx] = gfactor(z, x, Rₖ, l, σ)
        end
        show_progress && next!(p)
    end

    for (lidx, l) in enumerate(l)
        # G is symmetric along z if l is even and is anti-symmetric if l is odd
        sign = iseven(l) ? 1 : -1
        for k in 1:kmax, ij in eachindex(coords)
            ij <= onaxis && continue
            ij′ = ijmax - ij + 1 + onaxis
            G[ij′, k, lidx] = sign*G[ij, k, lidx]
        end
    end

    indices = Vector{CartesianIndex{2}}(undef, ijmax)
    for (ij, (_, _, j, i, j′)) in enumerate(coords)
        indices[ij]  = CartesianIndex(j,  i)
        ij <= onaxis && continue
        ij′ = ijmax - ij + 1 + onaxis
        indices[ij′] = CartesianIndex(j′, i)
    end
    reshape(G, ijmax, :), indices, onaxis
end


"Return a factor of G matrix"
function gfactor(z, x, Rₖ, l, σ)
    s = sqrt(σ/2)
    if hypot(z, x) > Rₖ + 6s
        return 0.0
    end

    # Set the origin (0, 0) as the root of R′ vector
    # R′ points the vertical top when θ′ = 0°
    f = integrand(z, x, Rₖ, l, σ)
    tmax = sqrt(Rₖ + 10s - abs(x))
    rtol = sqrt(eps())
    atol = 1e-200
    g, err = quadgk(f, 0.0, tmax, rtol=rtol, atol=atol)::Tuple{Float64,Float64}
    @assert err < max(atol, rtol*norm(g)) @sprintf("Numerical integration didn't converge (%.1f, %.1f, %.1f, %d, %f)", z, x, Rₖ, l, σ)
    return 4*g
end


"""
    integrand(x::Real, z::Real, Rₖ::Real, l::Int, σ::Real)

Return the integrand of Eq. 4. The integrand is a little modified to improve
the robustness in numerical integration. The original equation is:
    gₖₗ(R′, θ′) = 2 ∫(r*fₖₗ(R, θ)/√(r² - x²))dr
where it integrates with respect to r from |x| to ∞. The problem is that the
integrand is singular wherever `r == x`. The modified equation is:
    gₖₗ(R′, θ′) = 4 ∫((|x| + t²)*fₖₗ(R, θ)/√(2|x| + t²))dt
                = 4 ∫(r*fₖₗ(R, θ)/√(r + |x|))dt
where `t = √(r - |x|)`, it integrates with respect to t from 0 to ∞.
The improved equation has the only one singularity at `x == r == 0`.
"""
function integrand(z::Real, x::Real, Rₖ::Real, l::Int, σ::Real)
    absx = abs(x)
    z² = z*z
    return t -> begin
        r = t*t + absx
        r² = r*r
        R = sqrt(z² + r²)
        cosθ = z/R
        r*fkl_rad(R, Rₖ, σ)*legendre(l, cosθ)/sqrt(r + absx)
    end
end


"The radial part of the basis function (Eq. 2)"
@inline function fkl_rad(R::Float64, Rₖ::Float64, σ::Real)
    ΔR = R - Rₖ
    exp(-(ΔR*ΔR)/σ)
end


"Ensure the symmetry of U matrix."
function balance!(U, onaxis)
    symhalf = (size(U, 1) - onaxis)÷2
    jmin = onaxis + 1
    jmid = onaxis + symhalf
    jmax = size(U, 1)
    for i in 1:size(U, 2)
        @assert isapprox(abs(U[jmid, i]), abs(U[jmid+1, i]); rtol=1e-5, atol=1e-12) "|U⁺| = $(U[jmid, i]), |U⁻| = $(abs(U[jmid+1, i]))"
        j′ = jmax
        for j in jmin:jmid
            sa = signbit(U[j,  i])
            sb = signbit(U[j′, i])
            u = (abs(U[j, i]) + abs(U[j′, i]))/2
            U[j,  i] = ifelse(sa, -u, u)
            U[j′, i] = ifelse(sb, -u, u)
            j′ -= 1
        end
    end
end


"Return true if the coordinate is inside the image"
isinside(c, img) = 1 <= c[1] <= size(img, 1) && 1 <= c[2] <= size(img, 2)


"Return the two-fold image with a certain axis"
function twofold(A::Matrix{T}; dims::Integer, rev::Bool=false) where {T<:Real}
    sd = size(A, dims)
    if sd == 1 || isempty(A)
        return copy(A)
    end
    sd1, sd2 = size(A)
    if dims == 1
        hsd1  = (sd1 + 1)÷2
        hsd1′ = (sd1÷2 + 1)
        B = Matrix{T}(undef, hsd1, sd2)
        i = 1:1:sd2
        j = 1:1:hsd1
        k = rev ? (hsd1:-1:1)   : (1:1:hsd1)
        l = rev ? (hsd1′:1:sd1) : (sd1:-1:hsd1′)
        @inbounds for i in i, (j, k) in zip(j, k)
            B[j, i] = A[k, i]
        end
        @inbounds for i in i, (j, l) in zip(j, l)
            B[j, i] += A[l, i]
        end
    else
        hsd2  = (sd2 + 1)÷2
        hsd2′ = (sd2÷2 + 1)
        B = Matrix{T}(undef, sd1, hsd2)
        i = 1:1:hsd2
        j = rev ? (hsd2:-1:1)   : (1:1:hsd2)
        k = rev ? (hsd2′:1:sd2) : (sd2:-1:hsd2′)
        l = 1:1:sd1
        @inbounds for (i, j) in zip(i, j), l in l
            B[l, i] = A[l, j]
        end
        @inbounds for (i, k) in zip(i, k), l in l
            B[l, i] += A[l, k]
        end
    end
    return B
end


"Return the coefficient matrix C by least squares fitting"
function leastsquaresfit(fit::Symbol, pbx::PBasex, P::Vector{T},
                         atol::Real, rtol::Real) where {T<:Real}
    U = pbx.U
    S = pbx.S
    V = pbx.V
    varP = variance(P)
    if fit == :OLS
        tol = max(rtol*maximum(S), atol)
        if minimum(S) <= tol
            # improve condition number
            S⁺ = Sinverse(S, tol)
            G⁺ = V*Diagonal(S⁺)*U'
        else
            # use cached G⁺
            G⁺ = pbx.G⁺
        end
        G′ = G⁺
    elseif fit == :WLS
        w  = Diagonal(@.(1 / sqrt(varP)))
        G  = U*Diagonal(S)*V'
        # FIXME: Is there any way to avoid `pinv` in the reconstruction phase?
        G⁺ = pinv(w*G; atol=atol, rtol=rtol)
        G′ = G⁺*w
    else
        ArgumentError("fit=$(fit) is not supported.") |> throw
    end
    C = G′*P
    varC = propagate_variance(varP, G′)
    C, varC
end

function Sinverse(S::Vector{T}, tol) where {T<:Real}
    index = S .> tol
    Sinv = zeros(T, length(S))
    Sinv[index] = one(T) ./ S[index]
    Sinv[findall(.!isfinite.(Sinv))] .= zero(T)
    Sinv
end


"Return variance vector assuming that the signal counts obey Poisson statistics"
function variance(P::AbstractArray{<:Real})
    all(P .≈ 0) && return one.(P)

    # Assuming that the signal count obeys Poisson statistics. If the count is
    # big, practically N > 10, it can be approximated to a normal distribution
    # with the standard deviation σP = √P, where P is the count of the pixel.
    # The weight for a residual is the inverse of the square of σP; w = 1/σP².
    σP² = abs.(P)

    # Consider that any pixel has a non-zero standard deviation σPₘᵢₙ.
    # It is estimated to the square root of the non-zero minimum of σP².
    # This is to avoid to obtain w == Inf.
    σPmin² = minimum(filter(x -> x > 0, σP²))
    σP²[σP² .== 0] .= σPmin²
    return σP²
end


"""
    propagate_variance(varP::Vector, G⁺)

Return the diagonal terms of the variance-covariance matrix of coefficient C
matrix. The variances of coefficient matrix C can be obtained as the diagonal
terms of its covariance matrix.
    varC = diag(G⁺*varP*transpose(G⁺))
"""
function propagate_variance(varP::Vector, G⁺)
    var = zeros(Float64, (size(G⁺, 1),))
    for j in 1:size(G⁺, 2), i in 1:size(G⁺, 1)
        var[i] += G⁺[i, j]^2*varP[j]
    end
    var
end


"""
    to_cartesian(F, R, θ, len)

Convert the polar coordinates to the Cartesian coordinates to obtain the center
slice of the 3D distribution.
"""
function to_cartesian(C, R, l, σ, len)
    h = len
    w = (len + 1)÷2
    halfslice = zeros(Float64, h, w)
    y0 = (len + 1)÷2
    x0 = iseven(len) ? 0.5 : 1.0
    Rmax = last(R)
    Cfrad = C'*fkl_rad.(R, R', σ)
    function F(k, l, cosθ)
        f = 0.0
        @inbounds for (lidx, l) in enumerate(l)
            f += Cfrad[lidx, k]*legendre(l, cosθ)
        end
        f
    end
    for x in 1:w, y in 1:h
        dy = y - y0
        dx = x - x0
        r = hypot(dy, dx)
        r > Rmax && continue
        r == 0 && continue
        k1, k2 = findk(r, R)
        cosθ = dy/r
        if k1 == k2
            Fyx = F(k1, l, cosθ)
        else
            F1 = F(k1, l, cosθ)
            F2 = F(k2, l, cosθ)
            a = (r - R[k1])/(R[k2] - R[k1])
            Fyx = F1 + a*(F2 - F1)
        end
        @inbounds halfslice[y, x] = Fyx
    end
    halfslice
end

"Binary-search the adjacent indices k1 and k2 safisfying R[k1] ≤ r ≤ R[k2]"
function findk(r, R)
    @assert first(R) ≤ r ≤ last(R)
    kmin = 1
    kmax = length(R)
    r == R[kmax] && return kmax, kmax
    r == R[kmin] && return kmin, kmin
    while kmin + 1 != kmax
        k = (kmin + kmax)÷2
        if @inbounds R[k] == r
            return k, k
        elseif @inbounds R[k] > r
            kmax = k
        else
            kmin = k
        end
    end
    return kmin, kmax
end


"Obtain a mirror image"
function mirror(half)
    height, halfwidth = size(half)
    if iseven(halfwidth)
        width = 2*halfwidth
        xc⁻ = halfwidth
        xc⁺ = halfwidth + 1
    else
        width = 2*halfwidth - 1
        xc⁻ = halfwidth
        xc⁺ = halfwidth
    end
    image = Matrix{Float64}(undef, (height, width))
    @inbounds for dx in 0:halfwidth-1, y in 1:height
        z = half[y, dx + 1]
        image[y, xc⁻ - dx] = z
        image[y, xc⁺ + dx] = z
    end
    image
end


"""
    PBasexImage{T} <: AbstractMatrix{T}

A reconstructed 3D distribution.

See also: [`radialdist`](@ref), [`speeddist`](@ref), [`energydist`](@ref),
          [`anisotropy`](@ref)
"""
struct PBasexImage{T<:Real} <: AbstractMatrix{T}
    arr::Matrix{T}
    len::Int
    rstep::Float64
    astep::Float64
    l::Vector{Int}
    σ::Float64
    C::Matrix{Float64}
    varC::Matrix{Float64}

    function PBasexImage(arr::AbstractMatrix{T}, len::Integer,
                           rstep::Real, astep::Real, l, σ::Real,
                           C::AbstractMatrix{T}, varC::AbstractMatrix{T}) where {T<:Real}
        l = [0; filter(x -> x > 0, sort(unique(l)))]
        new{T}(arr, len, rstep, astep, l, σ, C, varC)
    end
end

function Base.show(io::IO, ::MIME"text/plain", z::PBasexImage)
    @printf("  size     : %d × %d\n", z.len, z.len)
    @printf("  intensity: %9.3e", sum(z.arr))
end

Base.Matrix(invimg::PBasexImage) = copy(invimg.arr)
Base.Array(invimg::PBasexImage) = copy(invimg.arr)
Base.convert(::Type{T}, invimg::PBasexImage) where T<:Matrix = convert(T, invimg.arr)
Base.convert(::Type{AbstractMatrix{Float64}}, invimg::PBasexImage) = copy(invimg.arr)

# AbstractArray interfaces
Base.size(invimg::PBasexImage) = size(invimg.arr)
Base.getindex(invimg::PBasexImage, I...) = getindex(invimg.arr, I...)
Base.length(invimg::PBasexImage) = length(invimg.arr)
Base.axes(invimg::PBasexImage) = map(Base.OneTo, size(invimg.arr))
Base.strides(invimg::PBasexImage) = strides(invimg.arr)
Base.stride(invimg::PBasexImage, k::Integer) = stride(invimg.arr, k)


"""
    radialdist(invimg::PBasexImage)

Calculate the radial distribution of the reconstructed 3D distribution.
See also: [`PBasex`](@ref)
"""
function IonimageAnalysisCore.radialdist(invimg::PBasexImage)
    C = invimg.C
    R = radii(invimg.len, invimg.rstep)
    σ = invimg.σ
    I = zeros(Float64, length(R))
    @inbounds for m in eachindex(R)
        for k in eachindex(R)
            I[m] += C[k, 1]*fkl_rad(R[m], R[k], σ)
        end
        I[m] *= R[m]^2
    end
    R, I
end


"""
    speeddist(invimg::PBasexImage, ppm, tof, N)

Compute the speed distribution of the reconstructed 3D distribution.
See also: [`PBasex`](@ref)
"""
function IonimageAnalysisCore.speeddist(invimg::PBasexImage, ppm, tof, N)
    r, Ir = radialdist(invimg)
    speeddist(r, Ir, ppm, tof, N)
end


"""
    energydist(invimg::PBasexImage, ppm, tof, N, m, M)

Coumpute the energy distribution of the reconstructed 3D distribution.
See also: [`PBasex`](@ref)
"""
function IonimageAnalysisCore.energydist(invimg::PBasexImage, ppm, tof, N, m, M)
    v, Iv = speeddist(invimg, ppm, tof, N)
    energydist(v, Iv, m, M)
end


"""
    anisotropy(invimg::PBasexImage)

Compute the anisotropy parameters of the reconstructed 3D distribution.
See also: [`PBasex`](@ref)
"""
function IonimageAnalysisCore.anisotropy(invimg::PBasexImage)
    C = invimg.C
    varC = invimg.varC
    l = invimg.l
    σ = invimg.σ
    R = radii(invimg.len, invimg.rstep)
    b    = Matrix{Float64}(undef, length(R), length(l)-1)
    berr = Matrix{Float64}(undef, length(R), length(l)-1)
    for l in 2:length(l), m in eachindex(R)
        b[m, l-1], berr[m, l-1] = _anisotropy(C, varC, R[m], R, l, σ)
    end
    R, b, berr, l[2:end]
end

function _anisotropy(C, varC, R, Rk, l::Integer, σ)
    # NOTE: here `l` is not an order of Legendre polynomials,
    #       `l` is the index of it
    I = 0.0
    b = 0.0
    Ierr² = 0.0
    berr² = 0.0
    for k in eachindex(Rk)
        f = fkl_rad(R, Rk[k], σ)
        f² = f^2
        I += C[k, 1]*f
        b += C[k, l]*f
        Ierr² += varC[k, 1]*f²
        berr² += varC[k, l]*f²
    end

    # no signal
    I <= 0 && return NaN, NaN

    R² = R^2
    I *= R²
    b *= R²
    Ierr² *= R²
    berr² *= R²
    return  b/I, sqrt((1/I)^2*berr² + (b/I^2)^2*Ierr²)
end


"""
    F, θ, R = polarmap(invimg::PBasexImage)

Obtaine the 3D distribution in polar coordinates. `F` is the 3D distribution
, `θ` and `R` are the corresponding angles and radii.
"""
function polarmap(invimg::PBasexImage)
    len = invimg.len
    rstep = invimg.rstep
    astep = invimg.astep
    l = invimg.l
    σ = invimg.σ
    C = invimg.C
    R = radii(len, rstep)
    θ = 0.0:astep:180.0
    F = legendre.(l', cosd.(θ))*C'*fkl_rad.(R, R', σ)
    F, θ, R
end


include("file.jl")


end # module
