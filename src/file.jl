using H5Zblosc
using HDF5: h5open
using LinearAlgebra: Diagonal, svd

const PBASEX_FILE_VERSION = "1.0.0"


"""
    savepbasex(path::AbstractString, pbasex::PBasex)

Save the reconstructor to a file in HDF5 format using Blosc library for
compression.

The file can be loaded by [`loadpbasex`](@ref)

# Example

```julia-repl
julia> using PBasexInversion

julia> pbasex = PBasex(200)   # Build a reconstructor
PBasexInversion.PBasex[200x200]: l=[0, 2], σ=2.00

julia> savepbasex("pbasex_200.h5", pbasex)  # Save the reconstructor to a file
```
"""
function savepbasex(path::AbstractString, pbx::PBasex; compress=9, kwargs...)
    sU, parity = shrink(pbx)
    indarray = _to_matrix(pbx.indices)
    h5open(path, "w") do fid
        fid["sU", blosc=compress] = sU
        fid["parity"] = parity
        fid["S", blosc=compress] = pbx.S
        fid["V", blosc=compress] = pbx.V
        fid["indices", blosc=compress] = indarray
        fid["onaxis"] = pbx.onaxis
        fid["len"] = pbx.len
        fid["rstep"] = pbx.rstep
        fid["astep"] = pbx.astep
        fid["l"] = pbx.l
        fid["sigma"] = pbx.σ
        fid["__VERSION__"] = PBASEX_FILE_VERSION
    end
    return
end


"""
    loadpbasex(path::AbstractString)

Load a reconstructor from a file saved by [`savepbasex`](@ref).

# Example

```julia-repl
julia> using PBasexInversion

julia> pbasex = loadpbasex("pbasex_200.h5")  # Load the reconstructor
PBasexInversion.PBasex[200x200]: l=[0, 2], σ=2.00

julia> ret = pbasex(img)  # Reconstruct an image
PBasexInversion.Reconstructedimage: raw: 200x200, slice: 200x200
```
"""
function loadpbasex(path::AbstractString)
    h5open(path, "r") do fid
        sU = read(fid["sU"])
        parity = read(fid["parity"])
        S = read(fid["S"])
        V = read(fid["V"])
        indarray = read(fid["indices"])
        onaxis = read(fid["onaxis"])
        len = read(fid["len"])
        rstep = read(fid["rstep"])
        astep = read(fid["astep"])
        l = read(fid["l"])
        σ = read(fid["sigma"])

        U = expand(sU, parity, onaxis)
        G⁺ = V*Diagonal(1 ./ S)*U'
        indices = _extract_indices(indarray)
        PBasex(G⁺, U, S, V, indices, onaxis, len, rstep, astep, l, σ)
    end
end


# NOTE: Probably, U and V can be reduced these size more with orthonormality.
#       The problem is numeric errors. Is there any good or reasonable workaround?
"Shrink a U matrix by its symmetry to return a shrunk U matrix"
function shrink(pbx::PBasex)
    U = pbx.U
    symhalf = (size(U, 1) - pbx.onaxis)÷2
    height = pbx.onaxis + symhalf
    sU = U[1:height, :]

    parity = Vector{Bool}(undef, size(U, 2))
    for i in 1:size(U, 2)
        parity[i] = signbit(U[height, i]) == signbit(U[height+1, i])
    end

    sU, parity
end


"Expand a shrunk U matrix to return an original U matrix"
function expand(sU, parity, onaxis)
    symhalf = size(sU, 1) - onaxis
    U = Matrix{Float64}(undef, onaxis + 2*symhalf, size(sU, 2))
    U[1:size(sU, 1), :] .= sU[:, :]
    jmin = size(sU, 1) + 1
    jmax = size(U, 1)
    for (i, p) in enumerate(parity)
        k = size(sU, 1)
        for j in jmin:jmax
            if p
                U[j, i] =  sU[k, i]
            else
                U[j, i] = -sU[k, i]
            end
            k -= 1
        end
    end
    U
end


"Convert a vector of CartesianIndex to Matrix"
function _to_matrix(indices)
    indarray = zeros(Int, length(indices), 2)
    for (i, c) in enumerate(indices)
        indarray[i, 1] = c[1]
        indarray[i, 2] = c[2]
    end
    indarray
end


"Extract a vector of CartesianIndex from Matrix"
function _extract_indices(indarray)
    indices = CartesianIndex{2}[]
    sizehint!(indices, size(indarray, 1))
    for i in 1:size(indarray, 1)
        push!(indices, CartesianIndex(indarray[i, 1], indarray[i, 2]))
    end
    indices
end


"Return the building parameters and file version of a pbasex file"
function statpbasex(path::AbstractString)
    h5open(path, "r") do fid
        len = read(fid["len"])
        l = read(fid["l"])
        σ = read(fid["sigma"])
        rstep = read(fid["rstep"])
        astep = read(fid["astep"])
        if haskey(fid, "__VERSION__")
            ver = read(fid["__VERSION__"])
        else
            ver = nothing
        end
        (len = len, l = l, σ = σ, rstep = rstep, astep = astep, __VERSION__ = ver)
    end
end
