PBasexInversion.jl
=========

[![Build Status](https://gitlab.com/mnkmr/PBasexInversion.jl/badges/master/build.svg)](https://gitlab.com/mnkmr/PBasexInversion.jl/pipelines)
[![Coverage](https://gitlab.com/mnkmr/PBasexInversion.jl/badges/master/coverage.svg)](https://gitlab.com/mnkmr/PBasexInversion.jl/commits/master)

## Introduction

This package provides the functions to reconstruct a 3-dimensional distribution from the corresponding projected 2-dimensional image using **pBasex** method. [G. A. Garcia et al., *Rev. Sci. Instrum.* **75**, 2004, 4989-4996.]


## Install

Press `]` to start [Pkg REPL mode](https://docs.julialang.org/en/v1/stdlib/Pkg/), then execute:

```
(v1.1) pkg> registry add https://gitlab.com/mnkmr/MNkmrRegistry.git
(v1.1) pkg> add PBasexInversion
```


## Usage

### 0. Notice

The shown code snippets are executed in Normal REPL mode. Probably, you may know the green prompt.

```
julia>
```

Note that the `julia> ` prompt is **omitted** in the following sections. The texts starting with `#` symbol are comments. You **don't** need to type them. Those are inserted just for your information.

### 1. Load PBasexInversion module

```julia
using PBasexInversion
```

### 2. Obtain a reconstructor

At first, it is required to build a reconstructor. Determine the size of reconstructing square region, and assign the pixel length of a edge to the first parameter. It may take some time to build.

```julia
# obtain a reconstructor for 400 x 400 pixel region
pbasex = PBasex(400)
```

### 3. Reconstruct an 3D distribution

Pass an projection image as an array to the reconstructor. The both height and width of the image should be equal or larger than the reconstructor size. The center of the reconstructed region can be shift through the second parameter.

```julia
using DelimitedFiles
img = readdlm("path/to/tsv")

# reconstruct 3D distribution (recons) from an image array (img)
center = (200.5, 200.5)
recons = pbasex(img, center)
```

The `recons` is an instance of `PBasexInversion.PBasexImage` type. You can obtain the radial, speed, energy, and angular distributions by the methods `radialdist`, `speeddist`, `energydist`, and `anisotropy` functions.

```julia
using PBasexInversion,PyPlot

pbasex = PBasex(200)

# visualize the original image
imshow(img);

# smooth the image with a square averaging filter
# the results from noisy or weak signals sometimes largely improved by smoothing
smoothed = smooth(img, 3)
recons = pbasex(smoothed, (100.5, 100.5));

# visualize the center slice of reconstructed distribution
slice = Array(recons)
imshow(slice);

# plot the radial distribution in pixel unit
r, Ir = radialdist(recons)
clf();
plot(r, Ir);

# plot the speed distribution in m/s
ppm = 10e3   # pixel-per-meter (px/m)
tof = 10e-6  # time-of-flight in second
N   = 1.5    # magnification factor
v, Iv = speeddist(recons, ppm, tof, N)
clf();
plot(v, Iv)

# calculate the speed distribution from the radial distribution
v, Iv = speeddist(r, Ir, ppm, tof, N)
clf();
plot(v, Iv)

# plot the center-of-mass kinetic energy distribution in kJ/mol
# (For example, observing I⁺ of CH₃I -> CH₃ + I)
m = 127      # the mass of photofragment, I⁺
M = 142      # the mass of parent molecule, CH₃I
Et, IEt = energydist(recons, ppm, tof, N, m, M)
clf();
plot(Et, IEt)

# calculate the energy distribution from the speed distribution
Et, IEt = energydist(v, Iv, m, M)
clf();
plot(Et, IEt)

# plot β₂ along the radial axis
# NOTE that the  β₂ may not be reasonable if the corresponding radial distribution is too weak
r, b, berr = anisotropy(recons)
β = b[:, 1]
βerr = berr[:, 1]
clf();
errorbar(r, β, βerr, fmt="o");
```

The reconstructor is re-usable, that is, you are **not** necessarily to build a reconstructor every time you need it.

```julia
pbasex = PBasex(400)

# Those image size should be equal or larger than 400x400 px
img1 = readdlm("path/to/file1")
img2 = readdlm("path/to/file2")
img3 = readdlm("path/to/file3")

recons1 = pbasex(img1, (200.5, 200.5))
recons2 = pbasex(img2, (200.5, 200.5))
recons3 = pbasex(img3, (200.5, 200.5))
```

### 4. Save a reconstructor as a file

Building a reconstructor takes long time generally. To avoid wasting time, the reconstructor can be saved as a file by `savepbasex` function. The saved reconstructor can be re-loaded by `loadpbasex` function in a short-time. It will save time in future sessions.

```julia
# save a reconstructor
savepbasex("path/to/file", pbasex)
```

```julia
# load a reconstructor
pbasex = loadpbasex("path/to/file")
```

### 5. For more details...

See [Tutorial jupyter notebook](https://nbviewer.jupyter.org/urls/gitlab.com/mnkmr/PBasexInversion.jl/raw/master/HowToUse.ipynb).
