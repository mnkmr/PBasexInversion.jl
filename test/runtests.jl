using DelimitedFiles
using PBasexInversion
using Test

@testset "Unit tests" begin
    @test PBasexInversion.mirror(
        [1.  0.  0.
         0.  2.  0.
         0.  0.  3.
         0.  4.  0.
         5.  0.  0.]) ==
        [0.  0.  1.  0.  0.
         0.  2.  0.  2.  0.
         3.  0.  0.  0.  3.
         0.  4.  0.  4.  0.
         0.  0.  5.  0.  0.]
    @test PBasexInversion.mirror(
        [1.  0.
         0.  2.
         0.  0.
         0.  4.
         5.  0.]) ==
        [0.  1.  1.  0.
         2.  0.  0.  2.
         0.  0.  0.  0.
         4.  0.  0.  4.
         0.  5.  5.  0.]

    @test PBasexInversion.twofold(
        [ 1.   1.   1.
          2.   4.   8.
          3.   9.  27.], dims=1, rev=false) ==
        [ 4.  10.  28.
          4.   8.  16.]
    @test PBasexInversion.twofold(
        [ 1.   1.   1.
          2.   4.   8.
          3.   9.  27.], dims=2, rev=false) ==
        [ 2.   2.
         10.   8.
         30.  18.]
    @test PBasexInversion.twofold(
        [ 1.   1.   1.
          2.   4.   8.
          3.   9.  27.], dims=1, rev=true) ==
        [ 4.   8.  16.
          4.  10.  28.]
    @test PBasexInversion.twofold(
        [ 1.   1.   1.
          2.   4.   8.
          3.   9.  27.], dims=2, rev=true) ==
        [ 2.   2.
          8.  10.
         18.  30.]

    @test PBasexInversion.findk(1.0, 1:2) == (1, 1)
    @test PBasexInversion.findk(2.0, 1:2) == (2, 2)
    @test PBasexInversion.findk(1.5, 1:2) == (1, 2)
    @test PBasexInversion.findk(1.0, 1:3) == (1, 1)
    @test PBasexInversion.findk(2.0, 1:3) == (2, 2)
    @test PBasexInversion.findk(3.0, 1:3) == (3, 3)
    @test PBasexInversion.findk(1.5, 1:3) == (1, 2)
    @test PBasexInversion.findk(2.5, 1:3) == (2, 3)
    @test PBasexInversion.findk(1.0, 1:4) == (1, 1)
    @test PBasexInversion.findk(2.0, 1:4) == (2, 2)
    @test PBasexInversion.findk(3.0, 1:4) == (3, 3)
    @test PBasexInversion.findk(4.0, 1:4) == (4, 4)
    @test PBasexInversion.findk(1.5, 1:4) == (1, 2)
    @test PBasexInversion.findk(2.5, 1:4) == (2, 3)
    @test PBasexInversion.findk(3.5, 1:4) == (3, 4)
    @test PBasexInversion.findk(1.0, 1:5) == (1, 1)
    @test PBasexInversion.findk(2.0, 1:5) == (2, 2)
    @test PBasexInversion.findk(3.0, 1:5) == (3, 3)
    @test PBasexInversion.findk(4.0, 1:5) == (4, 4)
    @test PBasexInversion.findk(5.0, 1:5) == (5, 5)
    @test PBasexInversion.findk(1.5, 1:5) == (1, 2)
    @test PBasexInversion.findk(2.5, 1:5) == (2, 3)
    @test PBasexInversion.findk(3.5, 1:5) == (3, 4)
    @test PBasexInversion.findk(4.5, 1:5) == (4, 5)

    let img = zeros(3, 3)
        @test PBasexInversion.isinside((1, 1), img) == true
        @test PBasexInversion.isinside((2, 1), img) == true
        @test PBasexInversion.isinside((3, 1), img) == true
        @test PBasexInversion.isinside((1, 2), img) == true
        @test PBasexInversion.isinside((2, 2), img) == true
        @test PBasexInversion.isinside((3, 2), img) == true
        @test PBasexInversion.isinside((1, 3), img) == true
        @test PBasexInversion.isinside((2, 3), img) == true
        @test PBasexInversion.isinside((3, 3), img) == true
        @test PBasexInversion.isinside((0, 0), img) == false
        @test PBasexInversion.isinside((0, 1), img) == false
        @test PBasexInversion.isinside((4, 1), img) == false
        @test PBasexInversion.isinside((1, 0), img) == false
        @test PBasexInversion.isinside((1, 4), img) == false
        @test PBasexInversion.isinside((4, 4), img) == false
    end
end


function test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(recons) == size(recons.arr)
    @test getindex(recons, :) == getindex(recons.arr, :)
    @test length(recons) == length(recons.arr)
    @test axes(recons) == axes(recons.arr)
    @test strides(recons) == strides(recons.arr)
    @test stride(recons, 1) == stride(recons.arr, 1)
    @test stride(recons, 2) == stride(recons.arr, 2)

    r, Ir = radialdist(recons)
    @test all(0.0 .<= r .<= 70.01)
    @test length(r) == length(Ir) == 70
    @test 4 ≤ argmax(Ir[36:45]) ≤ 6
    @test 4 ≤ argmax(Ir[46:55]) ≤ 6
    @test 4 ≤ argmax(Ir[56:65]) ≤ 6

    v, Iv = speeddist(recons, 10e3, 1e-6, 2.0)
    @test all(0.0 .<= v .<=  3500.1)
    @test length(v) == length(Iv) == 70
    @test 4 ≤ argmax(Iv[36:45]) ≤ 6
    @test 4 ≤ argmax(Iv[46:55]) ≤ 6
    @test 4 ≤ argmax(Iv[56:65]) ≤ 6

    Et, IEt = energydist(recons, 15, 50, 10e3, 1e-6, 2.0)
    @test all(0.0 .<= Et .<=  131.251)
    @test length(Et) == length(IEt) == 70
    @test 4 ≤ argmax(IEt[36:45]) ≤ 6
    @test 4 ≤ argmax(IEt[46:55]) ≤ 6
    @test 4 ≤ argmax(IEt[56:65]) ≤ 6

    diff(arr, expect) = abs(sum(arr)/length(arr) - expect)
    r, b, berr, l = anisotropy(recons)
    β2 = b[:, 1]
    β2err = berr[:, 1]
    @test size(b, 1) == size(berr, 1) == size(r, 1)
    @test size(b, 2) == size(berr, 2) == size(l, 1)
    @test diff(β2[39:41], -1) < 0.01
    @test diff(β2[49:51],  0) < 0.01
    @test diff(β2[59:61],  2) < 0.01
    @test β2err[40] < 0.1
    @test β2err[50] < 0.1
    @test β2err[60] < 0.1
end


@testset "Reconstruction" begin
    # This image has three arcs (140 x 140)
    #   Arc1: r = 40 px, σ = 2, β = -1
    #   Arc2: r = 50 px, σ = 2, β = 0
    #   Arc3: r = 60 px, σ = 2, β = 2
    raw01 = readdlm(joinpath(@__DIR__, "test01.tsv"))
    len = minimum(size(raw01))
    pbasex = PBasex(len)

    recons = pbasex(raw01, (70.5, 70.5))
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw01)

    recons = pbasex(float.(raw01), (70.5, 70.5))
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw01)

    recons = pbasex(raw01, (70.5, 70.5); fit=:WLS)
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw01)

    recons = pbasex(float.(raw01), (70.5, 70.5); fit=:WLS)
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw01)

    F, θ, R = PBasexInversion.polarmap(recons)
    @test F isa Matrix{<:Real}
    @test size(F, 1) == length(θ)
    @test size(F, 2) == length(R)
    @test sum(F[:, 50]) < sum(F[:, 60])
    @test sum(F[1, :])  > sum(F[46, :])


    # This image has three arcs (141 x 141)
    #   Arc1: r = 40 px, σ = 2, β = -1
    #   Arc2: r = 50 px, σ = 2, β = 0
    #   Arc3: r = 60 px, σ = 2, β = 2
    raw02 = readdlm(joinpath(@__DIR__, "test02.tsv"))
    len = minimum(size(raw02))
    pbasex = PBasex(len)

    # The center coordinate is outside of the image
    @test_throws ArgumentError pbasex(raw02, (1, 1))
    @test_throws ArgumentError pbasex(raw02, (1, 71))
    @test_throws ArgumentError pbasex(raw02, (71, 1))

    # The reconstructed area get out of the image
    @test_throws ArgumentError pbasex(raw02, (70, 70))
    @test_throws ArgumentError pbasex(raw02, (72, 72))

    recons = pbasex(raw02, (71.0, 71.0))
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw02)

    recons = pbasex(raw02, (71, 71))
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw02)

    recons = pbasex(float.(raw02), (71.0, 71.0))
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw02)

    recons = pbasex(float.(raw02), (71, 71))
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw02)

    @test Matrix(recons) == Array(recons)
    @test Matrix(recons) == convert(Matrix, recons)
    @test Matrix(recons) == convert(Array{Float64,2}, recons)

    recons = pbasex(raw02, (71, 71); fit=:WLS)
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw02)

    recons = pbasex(float.(raw02), (71, 71); fit=:WLS)
    test_ReconstructedImage(recons)
    @test recons isa PBasexInversion.PBasexImage
    @test size(Matrix(recons)) == size(recons) == size(raw02)

    pbasexfile = tempname()
    savepbasex(pbasexfile, pbasex)
    pbasex2 = loadpbasex(pbasexfile)

    recons = pbasex(raw02, (71, 71))
    recons2 = pbasex2(raw02, (71, 71))
    test_ReconstructedImage(recons2)
    @test recons2 isa PBasexInversion.PBasexImage
    @test size(Matrix(recons2)) == size(Matrix(recons)) == size(raw02)
    @test Array(recons2) == Array(recons)


    # low quality image (141 x 141)
    #   Arc1: r = 40 px, σ = 1, β = -1
    #   Arc2: r = 50 px, σ = 1, β = 0
    #   Arc3: r = 60 px, σ = 1, β = 2
    raw03 = readdlm(joinpath(@__DIR__, "test03.tsv"))
    recons = pbasex(smooth(raw03, 3), (71, 71))
    r, b, berr = anisotropy(recons)
    β = b[:, 1]
    βerr = berr[:, 1]
    @test β[40] - βerr[40] ≤ -1 ≤ β[40] + βerr[40]
    @test β[50] - βerr[50] ≤  0 ≤ β[50] + βerr[50]
    @test β[60] - βerr[60] ≤  2 ≤ β[60] + βerr[60]


    # Complex image (141 x 141)
    #   β₁ = -0.2
    #   β₂ =  0.4
    #   β₃ =  0.6
    #   β₄ = -0.8
    raw04 = readdlm(joinpath(@__DIR__, "test04.tsv"))
    len = minimum(size(raw04))
    pbasex = PBasex(len, l=[1, 2, 3, 4])
    recons = pbasex(raw04, (71, 71))
    r, b, berr = anisotropy(recons)
    β₁ = b[60, 1]
    β₂ = b[60, 2]
    β₃ = b[60, 3]
    β₄ = b[60, 4]
    β₁err = berr[60, 1]
    β₂err = berr[60, 2]
    β₃err = berr[60, 3]
    β₄err = berr[60, 4]
    @test β₁ - β₁err ≤ -0.2 ≤ β₁ + β₁err
    @test β₂ - β₂err ≤  0.4 ≤ β₂ + β₂err
    @test β₃ - β₃err ≤  0.6 ≤ β₃ + β₃err
    @test β₄ - β₄err ≤ -0.8 ≤ β₄ + β₄err

    savepbasex(pbasexfile, pbasex)
    pbasex2 = loadpbasex(pbasexfile)

    recons2 = pbasex(raw04, (71, 71))
    r, b, berr = anisotropy(recons2)
    β₁ = b[60, 1]
    β₂ = b[60, 2]
    β₃ = b[60, 3]
    β₄ = b[60, 4]
    β₁err = berr[60, 1]
    β₂err = berr[60, 2]
    β₃err = berr[60, 3]
    β₄err = berr[60, 4]
    @test β₁ - β₁err ≤ -0.2 ≤ β₁ + β₁err
    @test β₂ - β₂err ≤  0.4 ≤ β₂ + β₂err
    @test β₃ - β₃err ≤  0.6 ≤ β₃ + β₃err
    @test β₄ - β₄err ≤ -0.8 ≤ β₄ + β₄err
end
